# Laundry

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


docker angular app

1. docker build -t laundry-app .


2. docker run -d -it -p 80:80/tcp --name laundry-app laundry-app:latest 


Dockerfile

### STAGE 1: Build ###
# We label our stage as 'builder'
FROM node:10-alpine as builder
COPY package.json package-lock.json ./
## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app/
## Move to /ng-app (eq: cd /ng-app)
WORKDIR /ng-app
# Copy everything from host to /ng-app in the container
COPY . .
## Build the angular app in production mode and store the artifacts in dist folder
ARG NG_ENV=prod
RUN npm run build
### STAGE 2: Setup ###
FROM nginx:1.17.1-alpine
## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/
## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
## From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /ng-app/dist/laundry /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
 

 -------

 .dockerignore

 .git
.firebase
.editorconfig
/node_modules
/e2e
/docs
.gitignore
*.zip
*.md


-----
create folder in project root  name nginx
create file inside folder named default.conf and paste below code

server {

  listen 80;

  sendfile on;

  default_type application/octet-stream;

  gzip on;
  gzip_http_version 1.1;
  gzip_disable      "MSIE [1-6]\.";
  gzip_min_length   1100;
  gzip_vary         on;
  gzip_proxied      expired no-cache no-store private auth;
  gzip_types        text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
  gzip_comp_level   9;


  root /usr/share/nginx/html;
  proxy_http_version 1.1;
  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection "upgrade";


  location / {
    try_files $uri $uri/ /index.html =404;
  }

}
 -----

!important put some times needed



/// mobile responsive


// Extra small devices (portrait phones, less than 576px)
// No media query for `xs` since this is the default in Bootstrap

// Small devices (landscape phones, 576px and up)
@media (min-width: 576px) {

 }

// Medium devices (tablets, 768px and up)
@media (min-width: 768px) {  }

// Large devices (desktops, 992px and up)
@media (min-width: 992px) {  }

// Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) {  }


//--------

// Extra small devices (portrait phones, less than 576px)
@media (max-width: 575.98px) {  }

// Small devices (landscape phones, 576px and up)
@media (min-width: 576px) and (max-width: 768px) {


  }


// Medium devices (tablets, 768px and up)
@media (min-width: 769px) and (max-width: 991.98px) {  }

// Large devices (desktops, 992px and up)
@media (min-width: 992px) and (max-width: 1199.98px) {  }

// Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) {  }





font-weight: 500; //medium