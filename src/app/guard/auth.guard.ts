
import { Injectable } from "@angular/core";
import {
  CanActivate,

  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { NzNotificationService } from 'ng-zorro-antd';
import { AuthService } from '../services/auth.service';
@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(public router: Router,
    private auth: AuthService,
    public toastr:NzNotificationService
  ) { }


  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
    this.toastr.info("Login Please!","")
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }
}
