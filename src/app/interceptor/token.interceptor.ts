import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from "@angular/common/http";
import { catchError, tap } from "rxjs/operators";
import { throwError } from "rxjs";
//   import { NgxSpinnerService } from "ngx-spinner";
import { NzNotificationService } from "ng-zorro-antd";
import { ErrorMessage } from "../utils/error-message";
import { Injectable } from "@angular/core";
import { NgxSpinnerService } from 'ngx-spinner';


@Injectable({ providedIn: "root" })
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    public spinner: NgxSpinnerService,
    public toastr: NzNotificationService,

  ) {


  }
  intercept(request: HttpRequest<any>, next: HttpHandler): any {
    //this.spinner.show();

    if (localStorage.getItem('laundry')!='')
       {
      // SET TOKEN TO HEADER
      // tslint:disable-next-line: max-line-length
      request = request.clone({
        setHeaders: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer "
          // "X-Restli-Protocol-Version" : '2.0.0' // For linked in
        },
      });
      this.spinner.hide()
    }
    else{
this.spinner.hide()
    }
    return next.handle(request).pipe(

      catchError((err: any): any => {
        console.log("error", err);
        try {
          if ((err && err.error.status == 0) || err.status == 0) {
            this.toastr.error("Error", "something went wrong !");
            this.spinner.hide();

          } else {
            // this.toastr.error("Error", (error && error.error.message) || (error && error.message));
            // tslint:disable-next-line: max-line-length
            this.toastr.error(
              "Error",
              ErrorMessage.message[
                (err && err.error.message) || (err && err.message)
              ]
                ? ErrorMessage.message[
                (err && err.error.message) || (err && err.message)
                ]
                : this.convertError(err && err.error.message) ||
                (err && err.message)

            );
            // this.toastr.error('Error', this.convertError(error && error.error.message) || (error && error.message));
            this.spinner.hide();
          }
          //   this.spinner.hide();
        } catch (e) {
          console.log(e);
          this.spinner.hide();
        }
        return throwError(err);
      })
    );
  }

  convertError(error: string) {
    const errorArray = error.split("_") || [];
    let stringValue = "";
    errorArray.forEach((x, i) => {
      if (i == 0) {
      }
      stringValue =
        stringValue + " " + (i == 0 ? this.capitalize(x) : x.toLowerCase());
    });
    return stringValue;
  }

  capitalize = (s: string) => {
    const value: any = s.toLowerCase() || "";
    return value.charAt(0).toUpperCase() + value.slice(1);
  };
}
