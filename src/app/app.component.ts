import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';
import { from } from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner'
import { WindowService } from './services/window.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isCollapsed = false;

public innerWidth:any;
  screenHeight: number;
  screenWidth: number;


@HostListener('window.resize',['$event'])

items=[
  1,2,3,4,56,
]

onResize(event?){
  this.screenHeight = window.innerHeight;
  console.log(this.screenHeight)
  this.screenWidth = window.innerWidth;
  console.log(this.screenWidth)
}

 constructor(public spinner:NgxSpinnerService,
  private windowService: WindowService

  ){
this.onResize()
 }

 ngOnInit() {
  this.spinner.hide()

  const width= this.windowService.windowRef.innerWidth;

}

ngAfterViewInit() {
  this.spinner.hide()
   }

}
