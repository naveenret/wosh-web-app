import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
  selectedValue = null;

  name:string;
mediaSubscription:Subscription


  listOfItem = ['jack', 'lucy'];
  index = 0;
  devixceXs: boolean;
  addItem(input: HTMLInputElement): void {
    const value = input.value;
    if (this.listOfItem.indexOf(value) === -1) {
      this.listOfItem = [...this.listOfItem, input.value || `New item ${this.index++}`];
    }
  }


  isCollapsed = false;


  visible = false;
  constructor(
    public translate:TranslateService,
    public router:Router,
    public mediaobserve:MediaObserver
  ) {
    this.mediaSubscription= mediaobserve.media$.subscribe((result:MediaChange)=>{
      console.log(result.mqAlias);


      this.devixceXs = result.mqAlias==='xs'?true:false

    })

    translate.addLangs(['en','nl']);
    translate.setDefaultLang('en')
  }

  ngOnInit() {

  }
  switchLang(lang:string){
    this.translate.use(lang)
  }

  setName(name){
    this.name=name
  }


logout(){
  this.router.navigate(['/login'])
  this.clearData()

}

clearData(){
  localStorage.clear();
}

}
