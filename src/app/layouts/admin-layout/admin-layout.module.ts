import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminLayoutComponent } from './admin-layout.component';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
CommonModule,
    AdminLayoutRoutes,
    SharedModule,
    TranslateModule,

  ],
  declarations: [AdminLayoutComponent,
  ]
})
export class AdminLayoutModule { }
