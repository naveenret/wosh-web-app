import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout.component';
import { AuthGuard } from '../../guard/auth.guard';

const routes: Routes = [
  {
    path:'',
    component:AdminLayoutComponent,
    canActivate:[AuthGuard],
    children:[
      // {
      //   path:'',
      //   redirectTo:'/admin-layout/customer-management',
      //   pathMatch:'full'
      // },
      {
        path:'',
        redirectTo:'/admin-layout/dashboard',
        pathMatch:'full'
      },
      {
        path:'dashboard',
        loadChildren:()=>import("../../pages/dashboard/dashboard.module").then(m=>m.DashboardModule)
      },
      {
        path:'customer-management',
        loadChildren:()=>import('../../pages/customer-management/customer-management.module').then(m=>m.CustomerManagementModule)
      },
      {
        path:'order-management',
        loadChildren:()=>import("../../pages/order-management/order-management.module").then(m=>m.OrderManagementModule)
      },
      {
        path:'priority',
        loadChildren:()=>import("../../pages/priority/priority.module").then(m=>m.PriorityModule)
      },
      {
        path:'step-go',
        loadChildren:()=>import("../../pages/step-go/step-go.module").then(m=>m.StepGoModule)
      },
      {
        path:'today-revenue',
        loadChildren:()=>import("../../pages/today-revenue/today-revenue.module").then(m=>m.TodayRevenueModule)

      },

      {
        path:'staff-management',
        loadChildren:()=>import("../../pages/staff-management/staff-management.module").then(m=>m.StaffManagementModule)
      }
    ]
   },
];

export const AdminLayoutRoutes = RouterModule.forChild(routes);
