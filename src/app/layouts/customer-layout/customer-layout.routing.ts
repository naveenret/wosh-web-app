import { CustomerLayoutComponent } from "./customer-layout.component";
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    component:CustomerLayoutComponent,
    children:[
      {
        path:'',
        redirectTo:'/customer-layout/customer-mng',
        pathMatch:'full'
      },
      {
        path:'customer-mng',
        loadChildren:()=>import('../../pages/customer-management/customer-management.module').then(m=>m.CustomerManagementModule)
      },
      {
        path:'order-management',
        loadChildren:()=>import("../../pages/order-management/order-management.module").then(m=>m.OrderManagementModule)
      },
      {
        path:'priority',
        loadChildren:()=>import("../../pages/priority/priority.module").then(m=>m.PriorityModule)
      },
      {
        path:'step-go',
        loadChildren:()=>import("../../pages/step-go/step-go.module").then(m=>m.StepGoModule)
      },
      {
        path:'today-revenue',
        loadChildren:()=>import("../../pages/today-revenue/today-revenue.module").then(m=>m.TodayRevenueModule)

      },
      {
        path:'staff-management',
        loadChildren:()=>import("../../pages/staff-management/staff-management.module").then(m=>m.StaffManagementModule)
      }
    ]
   },
];

export const CustomerLayoutRoutes = RouterModule.forChild(routes);
