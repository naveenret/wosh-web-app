import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core/lib/translate.service';

@Injectable({
  providedIn: 'root'
})
export class TranslatingService {

constructor(
  public translate:TranslateService
) { }
switchLang(lang:string){
  this.translate.use(lang)
}
}
