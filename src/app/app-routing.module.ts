import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  {path:'login',loadChildren:()=>import("./pages/login/login.module").then(m=>m.LoginModule)},
  {path:'register',loadChildren:()=>import("./pages/register/register.module").then(m=>m.RegisterModule)},
  {path:'forgot-password',loadChildren:()=>import("./pages/forgot-password/forgot-password.module").then(m=>m.ForgotPasswordModule)},
  {
    path:'sign-up',
    loadChildren:()=>import("src/app/pages/sign-up/sign-up.module").then(m=>m.SignUpModule)
  },
  { path: 'admin-layout', loadChildren: () => import('./layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule),
 },
  {path:'**',loadChildren:()=>import("./pages/page-not-found/page-not-found.module").then(m=>m.PageNotFoundModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
