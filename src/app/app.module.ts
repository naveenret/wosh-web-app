import { TokenInterceptor } from "./interceptor/token.interceptor";
import { IconsProviderModule } from "./icons-provider.module";

import { FullCalendarModule } from '@fullcalendar/angular';
import interactionPlugin from '@fullcalendar/interaction';
import { registerLocaleData, CommonModule } from '@angular/common';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { IconDefinition } from '@ant-design/icons-angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientJsonpModule, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { en_US, NzLayoutModule, NzMenuModule, NzNotificationService, NZ_I18N, NZ_ICONS } from 'ng-zorro-antd';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppComponent } from './app.component';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import en from '@angular/common/locales/en';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ConnectionServiceModule } from 'ng-connection-service';
import { FlexLayoutModule } from '@angular/flex-layout';


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin
]);


 registerLocaleData(en);

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])

@NgModule({
  imports: [
CommonModule,
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,

RouterModule,

    BrowserAnimationsModule,
    ScrollingModule,
    DragDropModule,
ChartsModule,
    AppRoutingModule,
FlexLayoutModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
ConnectionServiceModule,
IconsProviderModule,
NzLayoutModule,
NzMenuModule,
FullCalendarModule,
NgxSpinnerModule,

  ],

  exports:[
    FullCalendarModule,
    ChartsModule

  ],
  declarations: [ AppComponent ],
  bootstrap: [ AppComponent,

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,

],
  providers: [
    NzNotificationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    { provide: NZ_I18N, useValue: en_US }, { provide: NZ_ICONS, useValue: icons } ]
})
export class AppModule { }
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
