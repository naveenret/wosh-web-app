import { NzNotificationService } from 'ng-zorro-antd';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ErrorMapper {
  constructor(
    public toastr: NzNotificationService
  ) {

  }


  public showErrorByStatusCode(statusCode: number, customMsg?: any) {

    switch (statusCode) {

      // 400 FOR BAD REQUEST :: 
      case 400:
        if (customMsg != undefined && customMsg != null) {
          // this.toastr.error('Error', customMsg);
        } else {
          // this.toastr.error('Oops!', 'Someting seems to have broken. Please contact us or try again after sometime');
        }
        break;
      // 
      case 401:
        // if (customMsg != undefined && customMsg != null) {
        //   // this.toastr.error('Error', customMsg);
        // } else {
          // this.toastr.error('Oops!', 'Username or password is incorrect');
        // }
        break;
      // 
      case 403:
        if (customMsg != undefined && customMsg != null) {
          // this.toastr.error('Error', customMsg);
        } else {
          // this.toastr.error('Oops!', 'Someting seems to have broken. Please contact us or try again after sometime');
        }
        break;
      // DEFALUT ::
      default:
        // this.toastr.error('Oops!', "Try again Later");
    }

  }

  public showCustomError(customMsg: any) {
    // this.toastr.error('Error', customMsg);
  }




}