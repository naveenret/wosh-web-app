import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { NzModalService } from 'ng-zorro-antd';

import { BaseChartDirective, Color, Label } from 'ng2-charts';
import {Chart} from 'node_modules/chart.js';
declare var $;
@Component({
  selector: 'app-order-management',
  templateUrl: './order-management.component.html',
  styleUrls: ['./order-management.component.scss']
})
export class OrderManagementComponent implements OnInit {
 name:any="Jeans"
  constructor(
    public modalService:NzModalService,
    private cdr:ChangeDetectorRef
  ) {

  }



  myData =[
    {
      name: 'Jacket',
      src:'assets/Cloth/jacket.png',
      price:'$2.00'
    },
    {
      name: 'T- Shirt',
      src:'assets/Cloth/t-shirt.png',
      price:'$2.00'
    },
    {
      name: 'Shirt',
      src:'assets/Cloth/shirt.png',
      price:'$2.00'
    },
    {
      name: 'Pant',
      src:'assets/Cloth/phant.png',
      price:'$2.00'
    },
    {
      name: 'Shoes',
      src:'assets/Cloth/shoe.png',
      price:'$2.00'
    },
    {
      name: 'Inners',
      src:'assets/Cloth/inner.png',
      price:'$2.00'
    },
    {
      name: 'Ladies Top',
      src:'assets/Cloth/top.png',
      price:'$2.00'
    },
    {
      name: 'Suit',
      src:'assets/Cloth/suit.png',
      price:'$2.00'
    },
    {
      name: 'Jacket',
      src:'assets/Cloth/jacket.png',
      price:'$2.00'
    },
    {
      name: 'Jacket',
      src:'assets/Cloth/jacket.png',
      price:'$2.00'
    },
    {
      name: 'Jacket',
      src:'assets/Cloth/jacket.png',
      price:'$2.00'
    },

  ];

  checkout=[
    {
      name: 'Suit',
      src:'assets/icons/shirts.svg',
      price:'$4.00'
    },
    {
      name: 'T-shirt',
      src:'assets/icons/shirts.svg',
      price:'$4.00'
    },
    {
      name: 'Phant',
      src:'assets/icons/shirts.svg',
      price:'$4.00'
    },
    {
      
      name: 'Suit',
      src:'assets/icons/shirts.svg',
      price:'$4.00'
    },
    {
      name: 'Shoes',
      src:'assets/icons/shirts.svg',
      price:'$4.00'
    },
  ]
  



  ngOnInit() {
    this.cdr.detectChanges()
  }


  closeModal() {
    
    $("#orderForm").modal("hide");
    
  }

  openOrderForm() {
    $("#orderForm").modal("show");
  }

  add(){

  }

}
