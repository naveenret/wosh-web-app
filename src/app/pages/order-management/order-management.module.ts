
import { OrderManagementRoutes } from './order-management.routing';


import { NgModule } from '@angular/core';

import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { OrderManagementComponent } from './order-management.component';




@NgModule({
  declarations: [
OrderManagementComponent

  ],
  imports: [

    OrderManagementRoutes,
    CommonModule,
    SharedModule
  ]
})
export class OrderManagementModule { }
