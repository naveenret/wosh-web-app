



import { OrderManagementComponent } from './order-management.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    component:OrderManagementComponent
   },
];

export const OrderManagementRoutes = RouterModule.forChild(routes);
