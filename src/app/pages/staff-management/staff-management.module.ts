import { StaffManagementRoutes } from "./staff-management.routing";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffManagementComponent } from './staff-management.component';

@NgModule({
  imports: [
    CommonModule,
   StaffManagementRoutes
  ],
  declarations: [StaffManagementComponent]
})
export class StaffManagementModule { }
