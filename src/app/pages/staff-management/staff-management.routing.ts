import { Routes, RouterModule } from '@angular/router';
import { StaffManagementComponent } from './staff-management.component';

const routes: Routes = [
  {
    path:'',
    component:StaffManagementComponent
   },
];

export const StaffManagementRoutes = RouterModule.forChild(routes);
