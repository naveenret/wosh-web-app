import { Routes, RouterModule } from '@angular/router';
import { PriorityComponent } from './priority.component';

const routes: Routes = [
  {
    path:'',
    component:PriorityComponent
   },
];

export const PriorityRoutes = RouterModule.forChild(routes);
