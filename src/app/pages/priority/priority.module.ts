import { PriorityRoutes } from "./priority.routing";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PriorityComponent } from './priority.component';



@NgModule({
  declarations: [PriorityComponent],
  imports: [
    CommonModule,
    PriorityRoutes
  ]
})
export class PriorityModule { }
