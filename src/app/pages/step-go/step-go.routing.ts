import { Routes, RouterModule } from '@angular/router';
import { StepGoComponent } from './step-go.component';

const routes: Routes = [
  {
    path:'',
    component:StepGoComponent
   },
];

export const StepGoRoutes = RouterModule.forChild(routes);
