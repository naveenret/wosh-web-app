import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepGoComponent } from './step-go.component';

describe('StepGoComponent', () => {
  let component: StepGoComponent;
  let fixture: ComponentFixture<StepGoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepGoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepGoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
