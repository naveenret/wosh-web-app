import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepGoComponent } from './step-go.component';
import { StepGoRoutes } from './step-go.routing';



@NgModule({
  declarations: [StepGoComponent],
  imports: [
    CommonModule,
    StepGoRoutes
  ]
})
export class StepGoModule { }
