import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found.component';
import { PageNotFoundRoutes } from './page-not-found.routing';

@NgModule({
  imports: [
    CommonModule,
    PageNotFoundRoutes
  ],
  declarations: [PageNotFoundComponent]
})
export class PageNotFoundModule { }
