import { Routes, RouterModule } from '@angular/router';
import { CustomerManagementComponent } from './customer-management.component';

const routes: Routes = [
  {
    path:'',
    component:CustomerManagementComponent
   },
];

export const CustomerManagementRoutes = RouterModule.forChild(routes);
