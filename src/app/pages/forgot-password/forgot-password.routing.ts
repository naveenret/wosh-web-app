import { ForgotPasswordComponent } from "./forgot-password.component";
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:"",
    component:ForgotPasswordComponent
   },
];

export const ForgotPasswordRoutes = RouterModule.forChild(routes);
