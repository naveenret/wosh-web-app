import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ForgotPasswordRoutes } from './forgot-password.routing';

@NgModule({
  imports: [
    CommonModule,
    ForgotPasswordRoutes
  ],
  declarations: [ForgotPasswordComponent]
})
export class ForgotPasswordModule { }
