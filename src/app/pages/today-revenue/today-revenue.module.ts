import { TodayRevenueRoutes } from "./today-revenue.routing";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodayRevenueComponent } from './today-revenue.component';



@NgModule({
  declarations: [TodayRevenueComponent],
  imports: [
    CommonModule,
    TodayRevenueRoutes
  ]
})
export class TodayRevenueModule { }
