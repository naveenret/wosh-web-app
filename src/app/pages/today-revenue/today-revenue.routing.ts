import { Routes, RouterModule } from '@angular/router';
import { TodayRevenueComponent } from './today-revenue.component';

const routes: Routes = [
  {
    path:'',
    component:TodayRevenueComponent
    },
];

export const TodayRevenueRoutes = RouterModule.forChild(routes);
