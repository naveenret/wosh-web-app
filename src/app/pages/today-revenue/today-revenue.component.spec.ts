import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodayRevenueComponent } from './today-revenue.component';

describe('TodayRevenueComponent', () => {
  let component: TodayRevenueComponent;
  let fixture: ComponentFixture<TodayRevenueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodayRevenueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodayRevenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
