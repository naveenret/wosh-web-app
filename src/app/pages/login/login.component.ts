import { TranslateService } from "@ngx-translate/core";
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import  { Router } from '@angular/router'
import { NzNotificationService } from 'ng-zorro-antd';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  value;
  validateForm!: FormGroup;
  token: string='mytoken';

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  constructor(private fb: FormBuilder,
    public toastr:NzNotificationService,
    public router:Router,
    public cdr:ChangeDetectorRef,
    public translate:TranslateService) {
      translate.addLangs(['en','nl']);
      translate.setDefaultLang('en')

    }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
  switchLang(lang:string){
    this.translate.use(lang)
  }

  login(){

    this.router.navigate(['/admin-layout/dashboard']);
    this.toastr.success('Success','welcome to laundry')
    localStorage.setItem('laundry',this.token)
  }

  forgot(){
    this.router.navigate(['/forgot-password'])
  }


  ngAfterViewInit(){
    this.cdr.detectChanges();
  }

  signUp(){
  this.router.navigate(['/sign-up'])
  }
}
