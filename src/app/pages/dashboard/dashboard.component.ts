import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';

import { CalendarOptions } from '@fullcalendar/angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { Color, Label, MultiDataSet } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  loading: true;

  doughnutChartLabels: Label[] = [
    'Dry Cleaning',
    'Ioning',
    'Washing',
    'Stitching',
    'Packaging',
  ];

  doughnutChartData: MultiDataSet = [[55, 25, 20, 52, 54]];
  doughnutChartType: ChartType = 'doughnut';

  public doughnutChartColors: Color[] = [
    {
      backgroundColor: ['#0067FF ', '#FFC935', '#EF435D', '#BF5BFF', '#58D0FF'],
    },
  ];
  public options: any = {
    legend: {
      position: 'right' ,
    labels: {
			usePointStyle: true,
			fontFamily: 'Roboto-Regular',
			fontSize: 9,
			fontColor: '#637783',
		}},
  };
  // ------

  barChartOptions: ChartOptions = {
    responsive: true,
    legend: { position: 'top',
  display:false },

    scales: {
      xAxes:[
        {
          
          gridLines: {
            display: false,
          color:"#637783",
          
            drawOnChartArea: false

          },
        }
      ],
      yAxes: [
        {
          gridLines: {
            display: false,

            drawOnChartArea: false

          },
          display: true,
          ticks: {
            max: 750,
            min: 0,
            stepSize: 250          },
        },
      ],
    },
  };

  barChartLabels: Label[] = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [300, 26, 150, 36, 200, 600, 25], },
  ];

  public barChartColors: Color[] = [
    { backgroundColor: '#27AAE1' },
    { backgroundColor: '#27AAE1' },
  ];
  myColors = [{ backgroundColor: ['#27AAE1'] }];

  // tslint:disable-next-line: comment-format
  //-----

  optionList = [
    { label: 'Last Week', value: 'last' },
    { label: 'This Week', value: 'this' },
  ];

  selectedValue = { label: 'Last Week', value: 'last' };
  canvas: any;
  ctx: any;
  chartOptions: {
    series: number[];
    chart: { type: string };
    labels: string[];
    responsive: {
      breakpoint: number;
      options: { chart: { width: number }; legend: { position: string } };
    }[];
  };

  update() {
    this.elementRef.nativeElement.style.setProperty('color', 'red');
  }

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    headerToolbar: {
      left: 'prev',
      center: 'title',
      right: 'next',
    },
    dayMaxEvents: false, // allow "more" link when too many events
    events: [
      // {
      //   title: '',
      //   date: '2020-09-11',
      //   color: '#FFA826',
      // },
      // {
      //   title: '',
      //   date: '2020-10-08',
      //   color: '#00C149',
      // },
    ],

  };

  handleDateClick(arg) {
    alert('date click! ' + arg.dateStr);
  }

  data = [23, 45, 67, 89];
  title = [
    {
      name: 'Stop & Go ',
      value: '25',
    },
    {
      name: 'Priority',
      value: '25',
    },
    {
      name: 'New Service',
      value: '25',
    },
    {
      name: 'Payment Today',
      value: '25',
    },
  ];

  ngOnInit() {
    this.calender();
  }

  constructor(
    public elementRef: ElementRef,
    public spinner: NgxSpinnerService
  ) {}

  calender() {}
}
