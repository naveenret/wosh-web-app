import { NgxSpinnerModule } from "ngx-spinner";



import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { SharedModule } from '../../shared/shared.module';
import { FullCalendarModule } from '@fullcalendar/angular';


@NgModule({
  imports: [
    CommonModule,
    DashboardRoutes,
    SharedModule,
FullCalendarModule,


  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
